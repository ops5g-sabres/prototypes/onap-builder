/*
 * Creating an openstack test cluster.
 */
function Compute(name, ncore, nmem, space) {
  return {
    name: name,
    image: "ubuntu-2004",
    disks: [ { "size": space, "dev": "vdb", "bus": "virtio" } ],
    cpu: {
      cores: ncore,
      "passthru": true,
    },
    memory: { capacity: GB(nmem) },
  };
}

topo = {
  "name": "test",
  "nodes": [ Compute("onap", 64, 256, "320G"), Compute("openstack", 8, 24, "80G")],
  "switches": [],
  "links": [
     v2v("onap", 1, "openstack", 1, { mac: { onap: '04:70:00:00:01:01', openstack: '04:70:00:00:00:01' } }),  
  ]
}

function v2v(a, ai, b, bi, props={}) {
            lnk = Link(a, ai, b, bi, props);
            lnk.v2v = true;
            return lnk;
}

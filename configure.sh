#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
    echo "requires root access to run ansible playbook"
    exit 1
fi

# TODO: this can only be run once - need to make it idemptotent
ansible-playbook -i .rvn/ansible-hosts ansible/update_all_host_packages.yml
ansible-playbook -i .rvn/ansible-hosts ansible/partition_disks.yml
ansible-playbook -i .rvn/ansible-hosts ansible/install_onap.yml

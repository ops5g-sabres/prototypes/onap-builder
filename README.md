# onap-builder

Builds the onap environment on a single virtual machine.

## Dependencies

[raven](https://gitlab.com/ops5g-sabres/raven)

[ansible](https://github.com/ansible/ansible)

## Building

```
./run.sh
```

## Installing ONAP

```
./configure.sh
```

### Onap configuration for slicing

Who knows.

## Installing Openstack

TODO

